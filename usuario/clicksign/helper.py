import requests
import os


def geradocumento(dados, template):
    token = os.environ.get('TOKENCLICKSIGN')
    url = "https://app.clicksign.com/api/v1/templates/{}/documents?access_token={}".format(template, token)
    data = {
        "document": {
            "path": "/contrato_02/documento.docx",
            "template": {
                "data": dados
            }
        }
    }

    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(url=url, json=data, headers=headers)
    print(r.text)
    rt = r.json()
    print(rt)
    return rt['document']['key']


def criarassinante(email, phone_number, name, document, birthday):
    token = os.environ.get('TOKENCLICKSIGN')
    url = "https://app.clicksign.com/api/v1/signers?access_token={}".format(token)
    data = {
        "signer": {
            "email": email,
            "phone_number": phone_number,
            "auths": [
                "email"
            ],
            "name": name,
            "documentation": document,
            "birthday": birthday,
            "has_documentation": True
        }
    }
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(url=url, json=data, headers=headers)
    print(r.text)
    rt = r.json()
    return rt['signer']['key']


def assinanteparadocumento(assinante, documento):
    token = os.environ.get('TOKENCLICKSIGN')
    url = "https://app.clicksign.com/api/v1/lists?access_token={}".format(token)
    data = {
        "list": {
            "document_key": documento,
            "signer_key": assinante,
            "sign_as": "sign"
        }
    }
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(url=url, json=data, headers=headers)
    print(r.text)
    rt = r.json()
    return rt['list']['request_signature_key'], rt['list']['url']


def enviaremail(requisicaodeassinatura, urlemail):
    token = os.environ.get('TOKENCLICKSIGN')
    url = "https://app.clicksign.com/api/v1/notifications?access_token={}".format(token)
    data = {
        "request_signature_key": requisicaodeassinatura,
        "message": "Por favor assine o documento.\n\nQualquer dúvida estou à disposição.\n\nAtenciosamente,\n",
        "url": urlemail
    }
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(url=url, json=data, headers=headers)
    print(r.text)
    return 1
