import boto3
from botocore.exceptions import ClientError


def enviaemail(destinario, assunto, corpo, rementente="no-reply@glebba.com.br"):
    SENDER = rementente
    RECIPIENT = destinario
    SUBJECT = assunto
    BODY_TEXT = corpo
    BODY_HTML = corpo
    CHARSET = "UTF-8"
    # CONFIGURATION_SET = "ConfigSet"
    AWS_REGION = "us-east-1"
    client = boto3.client('ses', region_name=AWS_REGION)
    try:
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    RECIPIENT,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,

        )
    except ClientError as e:
        print(e.response['Error']['Message'])
        return False
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])
        return response['MessageId']
