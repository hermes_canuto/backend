from django.urls import path

from . import views

urlpatterns = [

    path('', views.Usuario.as_view({'post': 'create'})),
    path('confirma', views.Usuario.as_view({'post': 'confirma'})),
    path('codigo/reenviar', views.Usuario.as_view({'post': 'reenviaEmailConfirmacao'})),
    path('<int:pk>/', views.Usuario.as_view({'get': 'retrieve'})),
    path('investidor/', views.Investidor.as_view({'get': 'retrieve'})),
    path('investidor/investimentos/', views.Investidor.as_view({'get': 'retrieveInvestimentos'})),

]


