import datetime
import hashlib

import boto3
import datetime
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from warrant import Cognito
from oferta.api.serializer import OfertaSerializer, OfertaContratoSerializer, OfertaSimpleSerializer
from num2words import num2words


from validate_docbr import CPF

from usuario.api.serializer import UsuarioSerializer, InvestidorSerializer, UsuarioInvestidorSerializer, InvestimentoSerializer
from usuario.models import Usuario as UsuarioDB, \
    Endereco as EnderecoDB, \
     Oferta as OfertaDB, \
     OfertaContrato as OfertaContratoDB, \
    Investidor as InvestidorDB, \
    ContaBancaria, Investimento, Oferta

from usuario.clicksign.helper import geradocumento, criarassinante, assinanteparadocumento, enviaremail


def encriptasenha(value):
    sha_signature = hashlib.sha256(value.encode()).hexdigest()
    return sha_signature

def numero_por_extenso(number_p):
    if number_p.find(',')!=-1:
        number_p = number_p.split(',')
        number_p1 = int(number_p[0].replace('.',''))
        number_p2 = int(number_p[1])
    else:
        number_p1 = int(number_p.replace('.',''))
        number_p2 = 0    

    if number_p1 == 1:
        aux1 = ' real'
    else:
        aux1 = ' reais'

    if number_p2 == 1:
        aux2 = ' centavo'
    else:
        aux2 = ' centavos'

    text1 = ''
    if number_p1 > 0:
        text1 = num2words(number_p1,lang='pt_BR') + str(aux1)
    else:
        text1 = ''

    if number_p2 > 0:
        text2 = num2words(number_p2,lang='pt_BR') + str(aux2) 
    else: 
        text2 = ''

    if (number_p1 > 0 and number_p2 > 0):
        result = text1 + ' e ' + text2
    else:
        result = text1 + text2

    return result

def numero_por_extenso_porcentagem(number_p):
    
    text1 = num2words(number_p,lang='pt_BR') + ' Porcento'

    return text1    



class Usuario(viewsets.ViewSet):

    def getdadosrequest(self, request):
        dados = request.data
        usuario = {
            'nome': dados['nome'],
            'sexo': dados['sexo'],
            'email': dados['email'],
            'telefone': dados['telefone'],
            'senha': dados['senha'],
            'data_nascimento': dados['data_nascimento'],
        }
        return usuario

    def create(self, request):
        try:
            dados = self.getdadosrequest(request)

            # # Verifica se o e-mail existe
            if UsuarioDB.objects.filter(email=dados['email']).count() > 0:
                content = {'Error': 'Email'}
                return Response(content, status=status.HTTP_400_BAD_REQUEST)

        except KeyError as error:

            content = {'Error': 'Chave não encontrada', "msg": str(error)}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)

        try:
            cog = Cognito('us-east-1_1GOGGacn9', '34adcmgur9e1nubjbf25c48ouq')
            cog.add_base_attributes(email=dados['email'])
            auth = cog.register(dados['email'], dados['senha'])
            print(auth)
        except Exception as err:
            erro = type(err).__name__
            if erro == 'UsernameExistsException':
                return Response({'Mensagem': 'Esse e-mail já está em uso.'}, status.HTTP_409_CONFLICT)
            else:
                print(err)
                return Response({'Mensagem': 'Ocorreu um erro ao tentar realizar o cadastro.'},
                                status.HTTP_400_BAD_REQUEST)

        dados['senha'] = encriptasenha(dados['senha'])

        u = UsuarioDB(**dados)
        u.save()
        content = {'usuario': u.id_usuario}
        return Response(content, status=status.HTTP_200_OK)

    def confirma(self, request):
        dados = request.data
        email = dados['email']
        code = dados['code']
        username = dados['username']

        try:
            u = Cognito('us-east-1_1GOGGacn9', '34adcmgur9e1nubjbf25c48ouq', username=email)
            retorno = u.confirm_sign_up(code, username=email)
            print(retorno)
            _usuario = UsuarioDB.objects.get(email=email)
            _usuario.emailverificado = True
            _usuario.cognitousername = username
            _usuario.save()
            return Response({}, status=status.HTTP_200_OK)
        except UsuarioDB.DoesNotExist:
            return Response({'Mensagem': 'Erro ao confirmar o email.'}, status.HTTP_400_BAD_REQUEST)

    def reenviaEmailConfirmacao(self, request):
        dados = request.data
        email = dados['email']

        client = boto3.client('cognito-idp')

        response = client.resend_confirmation_code(
            ClientId='34adcmgur9e1nubjbf25c48ouq',
            Username=email
        )

        return Response({}, status=status.HTTP_200_OK)

    def retrieve(self, request, pk=None):
        try:
            authorization = request.META.get('HTTP_AUTHORIZATION', None)

            client = boto3.client('cognito-idp')
            response = client.get_user(
                AccessToken=authorization
            )
            queryset = UsuarioDB.objects.get(cognitousername=response['Username'])
            serializer = UsuarioSerializer(queryset)
            return Response(serializer.data)
        except UsuarioDB.DoesNotExist:
            return Response({'Mensagem': 'Usuario Não Encontrada'}, status.HTTP_400_BAD_REQUEST)


class Logar(viewsets.ViewSet):

    def create(self, request):
        dados = request.data
        email = dados['email']
        senha = dados['senha']

        try:
            u = Cognito('us-east-1_1GOGGacn9', '34adcmgur9e1nubjbf25c48ouq', username=email)
            u.authenticate(password=dados['senha'])
            _usuario = UsuarioDB.objects.get(email=email)
            _usuario.contador = _usuario.contador + 1
            _usuario.ultimo_login = datetime.datetime.today()
            _usuario.save()
            return Response({'token': u.access_token}, status=status.HTTP_200_OK)
        except Exception as err:

            erro = type(err).__name__

            if erro == 'UserNotConfirmedException':
                return Response({'Mensagem': '', 'internal_error_code': 1001}, status.HTTP_400_BAD_REQUEST)
            elif erro == 'UserNotFoundException':
                return Response({'Mensagem': 'Usuário não encontrado.', 'internal_error_code': 1002},
                                status.HTTP_404_NOT_FOUND)
            else:
                return Response(
                    {'Mensagem': 'Ocorreu um erro ao tentar realizar o login.', 'internal_error_code': 1000},
                    status.HTTP_400_BAD_REQUEST)


class Investidor(viewsets.ViewSet):

    def getdadosusuario(self, request):
        dados = request.data
        dados = dados['usuario']
        usuario = {
            'nome': dados['nome'],
            'data_nascimento': dados['data_nascimento'],
            'email': dados['email'],
            'celular': dados['celular'],

        }
        return usuario

    def getdadosendereco(self, request):
        dados = request.data
        dados = dados['usuario']['endereco']
        endereco = {
            'pais': dados['pais'],
            'cep': dados['cep'],
            'logradouro': dados['logradouro'],
            'complemento': dados['complemento'],
            'numero': dados['numero'],
            'bairro': dados['bairro'],
            'cidade': dados['cidade'],
            'estado': dados['estado']
        }
        return endereco

    def getdadobanco(self, request):
        dados = request.data
        dados = dados['usuario']['dados_bancarios']
        dadosbancarios = {
            'banco': dados['banco'],
            'agencia': dados['agencia'],
            'conta': dados['conta'],
        }
        return dadosbancarios

    def getdadoperfilinvestidor(self, request):
        dados = request.data
        dadospi = dados['perfil_investidor']
        dados = dados['usuario']
        cpf = dados['cpf'].replace('-', '').replace('.', '')
        perfilinvestidor = {
            'cpf': cpf,
            'nacionalidade': dados['nacionalidade'],
            'estado_civil': dados['estado_civil'],
            'profissao': dados['profissao'],
            'num_documento': dados['rg'],
            'tipo_documento': 'RG',
            'orgao_emissor': dados['orgao_emissor'],
            'estado_emissor': "-",
            'tipo_investidor': dadospi['tipo_investidor'],
        }
        return perfilinvestidor

    def getdadoinvestimento(self, request):
        dados = request.data
        dadospi = dados['perfil_investidor']
        dados = dados['investimento']
        investimento = {
            'valor_investido': dados['valor_investido'],
            'valor_investimentos_anteriores': dadospi['valor_investimentos_anteriores'],
        }
        return investimento    

    def getdadocompostoinvestimentocomposto(self, request):
        dados = request.data
        dadospi = dados['perfil_investidor']
        dados = dados['investimento']
        investimento = {
            'valor_investido': dados['valor_investido'],
            'valor_investido_mask': dados['valor_investido_mask'],
            'unidades_investimento': dados['unidades_investimento'],
            'porcentagem_investimento': dados['porcentagem_investimento'],
            'valor_investimentos_anteriores': dadospi['valor_investimentos_anteriores'],
        }
        return investimento        

    def retrieve(self, request, pk=None):
        try:
            authorization = request.META.get('HTTP_AUTHORIZATION', None)

            client = boto3.client('cognito-idp')
            response = client.get_user(
                AccessToken=authorization
            )
            queryset = UsuarioDB.objects.get(cognitousername=response['Username'])
            serializer = UsuarioInvestidorSerializer(queryset)
            
            return Response(serializer.data)
        except UsuarioDB.DoesNotExist:
            return Response({'Mensagem': 'Usuario Não Encontrada'}, status.HTTP_400_BAD_REQUEST)

    def retrieveInvestimentos(self, request, pk=None):
        try:
            authorization = request.META.get('HTTP_AUTHORIZATION', None)

            client = boto3.client('cognito-idp')
            response = client.get_user(
                AccessToken=authorization
            )
            querysetInvestidor = UsuarioDB.objects.get(cognitousername=response['Username'])
            serializerInvestidor = UsuarioInvestidorSerializer(querysetInvestidor)

        except UsuarioDB.DoesNotExist:
            return Response({'Mensagem': 'Usuario Não Encontrada'}, status.HTTP_400_BAD_REQUEST)      

        queryset = Investimento.objects.filter(id_investidor=serializerInvestidor.data['id'])
        serializer = InvestimentoSerializer(instance=queryset, many=True)
            
        return Response(serializer.data)      

    def create(self, request, pk=None):
        # try:
        
        # Busca usuario pelo token e valida se esta ativo
        authorization = request.META.get('HTTP_AUTHORIZATION', None)

        try:
            client = boto3.client('cognito-idp')
            response = client.get_user(
                AccessToken=authorization
            )
            _usuario = UsuarioDB.objects.get(cognitousername=response['Username'])
        except Exception as err:
            return Response({'Mensagem': 'Usuario não encontrato'}, status.HTTP_404_NOT_FOUND)

        pi = self.getdadoperfilinvestidor(request)
        cpf = CPF()    


        e = self.getdadosendereco(request)
        cb = self.getdadobanco(request)
        i = self.getdadoinvestimento(request)
        ic = self.getdadocompostoinvestimentocomposto(request);
        
        #Valida CPF
        if cpf.validate(pi['cpf']):
            if InvestidorDB.objects.filter(cpf=pi['cpf']).count() == 0:
                pi['id_usuario'] = _usuario
                _investidor = InvestidorDB(**pi)
                _investidor.save() 

                e['id_investidor'] = _investidor
                _endereco = EnderecoDB(**e)
                _endereco.save()

                cb['id_investidor'] = _investidor
                _contabancaria = ContaBancaria(**cb)
                _contabancaria.save()

            else:
                _investidor = InvestidorDB.objects.get(cpf=pi['cpf'])
                #TODO se já existir no banco, fazer update dos dados

        else:
            content = {'Error': "CPF inválido"}
            return Response(content, status=status.HTTP_409_CONFLICT)

        

        #Busca oferta e contrato pro perfil do investidor
        try:
            queryset = OfertaDB.objects.get(id=pk)
            oferta = OfertaSimpleSerializer(queryset)
        except OfertaDB.DoesNotExist:
            return Response({'Mensagem': 'Oferta Não Encontrada'}, status.HTTP_400_BAD_REQUEST)


        try:
            querysetContrato = OfertaContratoDB.objects.get(id_oferta=pk,tipo_investidor=pi['tipo_investidor'])
            ofertaContrato = OfertaContratoSerializer(querysetContrato)
        except OfertaContratoDB.DoesNotExist:
            return Response({'Mensagem': 'Oferta Não Encontrada'}, status.HTTP_400_BAD_REQUEST)    

        

        i['id_investidor'] = _investidor

        i['id_oferta'] = Oferta.objects.get(pk=1)
        _investimento = Investimento(**i)
        _investimento.status = 'I'
        _investimento.save()
        

        dados = self.getdadosusuario(request)

        currentDT = datetime.datetime.now()

        dadosContrato = {
            "Nome Completo": dados['nome'],
            "Nascionalidade": pi['nacionalidade'],
            "Estado Civil": pi['estado_civil'],
            "Profissao": pi['profissao'],
            "Rg": pi['num_documento'],
            "Orgao": pi['orgao_emissor'],
            "CPF": pi['cpf'],
            "Cidade": e['cidade'],
            "Estado": e['estado'],
            "Endereco": e['logradouro'],
            "Numero": e['numero'],
            "Complemento": e['complemento'],
            "Bairro": e['bairro'],
            "Cep": e['cep'],
            "Valor Investimento": ic['valor_investido_mask'],
            "Valor Investimento por Extenso": numero_por_extenso(str(i['valor_investido'])),
            "Valor Investimento Anterior": i['valor_investimentos_anteriores'],
            "Valor Investimento Anterior Extenso": numero_por_extenso(str(i['valor_investimentos_anteriores'])),
            "Porcentagem Investimento": str(ic['porcentagem_investimento']).replace(".", ",") + '%',
            "Porcentagem Investimento por Extenso": numero_por_extenso_porcentagem(str(ic['porcentagem_investimento'])),
            "Cidade Emissao": "São Paulo",
            "Dia": currentDT.day,
            "Mes": currentDT.month,
            "Ano": currentDT.year
        }

        # ClickSign 
        documento = geradocumento(dados=dadosContrato, template=ofertaContrato.data['clicksign_key'])

        assinante = criarassinante(_usuario.email, _usuario.celular, dados['nome'], pi['cpf'], '')

        requisicaodeassinatura, urlemail = assinanteparadocumento(assinante, documento)

        rt = enviaremail(requisicaodeassinatura, urlemail)

        content = {'usuario': _usuario.id,
                   "investidor": _investidor.id,
                   "investimento": _investimento.id,
                   'documento': documento,
                   'assinante': assinante,
                   'requisicaodeassinatura': requisicaodeassinatura
        }

        _investimento.status = 'AC'
        _investimento.save()


        return Response(content, status=status.HTTP_200_OK)
