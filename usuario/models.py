from django.db import models
import uuid


class Usuario(models.Model):
    id = models.AutoField(primary_key=True)
    id_usuario = models.UUIDField(default=uuid.uuid4, editable=False)
    nome = models.CharField(max_length=100)
    sexo = models.CharField(max_length=1, blank=True, null=True)
    senha = models.CharField(max_length=100)
    data_nascimento = models.DateField(blank=True, null=True)
    email = models.CharField(max_length=100)
    data_inativo = models.DateTimeField(blank=True, null=True)
    telefone = models.CharField(blank=True, null=True, max_length=100)
    celular = models.CharField(blank=True, null=True, max_length=100)
    mfa = models.IntegerField(blank=True, null=True)
    url_foto = models.CharField(max_length=250, blank=True, null=True)
    data_registro = models.DateTimeField(auto_now_add=True)
    ultimo_login = models.DateTimeField(blank=True, null=True)
    contador = models.IntegerField(blank=True, null=True, default=0)
    emailverificado = models.IntegerField(blank=True, null=True)
    cognitousername = models.CharField(blank=True, null=True, max_length=100)

    class Meta:
        managed = True
        db_table = 'usuario'


class Investidor(models.Model):
    id = models.AutoField(primary_key=True)
    id_investidor = models.UUIDField(default=uuid.uuid4, editable=False)
    id_usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, db_column='id_usuario', related_name='investidor')
    cpf = models.CharField(unique=True, max_length=11)
    nacionalidade = models.CharField(max_length=60, blank=True, null=True)
    estado_civil = models.CharField(max_length=40, blank=True, null=True)
    profissao = models.CharField(max_length=50, blank=True, null=True)
    tipo_investidor = models.CharField(max_length=15)
    tipo_documento = models.CharField(max_length=3)
    num_documento = models.CharField(max_length=20)
    orgao_emissor = models.CharField(max_length=10)
    estado_emissor = models.CharField(max_length=2)

    class Meta:
        managed = True
        db_table = 'investidor'


class Endereco(models.Model):
    id = models.AutoField(primary_key=True)
    id_endereco = models.UUIDField(default=uuid.uuid4, editable=False)
    id_investidor = models.ForeignKey(Investidor, on_delete=models.CASCADE, db_column='id_investidor', related_name='endereco')
    cep = models.CharField(max_length=10, blank=True, null=True)
    logradouro = models.CharField(max_length=100)
    complemento = models.CharField(max_length=50, blank=True, null=True)
    numero = models.IntegerField(blank=True, null=True)
    bairro = models.CharField(max_length=100, blank=True, null=True)
    cidade = models.CharField(max_length=100, blank=True, null=True)
    estado = models.CharField(max_length=2, blank=True, null=True)
    pais = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'endereco'


class Empresa(models.Model):
    id = models.AutoField(primary_key=True)
    # id_empresa = models.UUIDField(default=uuid.uuid4, editable=False)
    nome = models.CharField(max_length=50)
    logo_url = models.CharField(max_length=250, blank=True, null=True)
    site = models.CharField(max_length=250, blank=True, null=True)
    descricao_html = models.CharField(max_length=400, blank=True, null=True)
    razao_social = models.CharField(max_length=70, blank=True, null=True)
    cnpj = models.CharField(max_length=25)
    mercado_atuacao = models.CharField(max_length=100, blank=True, null=True)
    regiao_atuacao = models.CharField(max_length=100, blank=True, null=True)
    ano_fundacao = models.DateField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'empresa'


class Empreendimento(models.Model):
    id = models.AutoField(primary_key=True)
    id_empreendimento = models.UUIDField(default=uuid.uuid4, editable=False)
    id_empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, db_column='id_empresa')
    nome = models.CharField(max_length=100, blank=True, null=True)
    descricao = models.CharField(max_length=2000, blank=True, null=True)
    logo_url = models.CharField(max_length=250, blank=True, null=True)
    site = models.CharField(max_length=250, blank=True, null=True)
    status = models.CharField(max_length=2, blank=True, null=True)
    cidade = models.CharField(max_length=100, blank=True, null=True)
    uf = models.CharField(max_length=2, blank=True, null=True)
    video_url = models.CharField(max_length=100, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'empreendimento'


class Oferta(models.Model):
    id = models.AutoField(primary_key=True)
    id_oferta = models.UUIDField(default=uuid.uuid4, editable=False)
    id_empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, db_column='id_empresa', related_name='ofertaempresas')
    id_empreendimento = models.ForeignKey(Empreendimento, on_delete=models.CASCADE, db_column='id_empreendimento',
                                          related_name='ofertaempreendimentos')
    nome = models.CharField(max_length=100, blank=True, null=True)
    descricao_html = models.CharField(max_length=2000, blank=True, null=True)
    logo_url = models.CharField(max_length=250, blank=True, null=True)
    site = models.CharField(max_length=250, blank=True, null=True)
    status = models.CharField(max_length=2, blank=True, null=True)
    data_inicio_oferta = models.DateField(blank=True, null=True)
    data_termino_oferta = models.DateField(blank=True, null=True)
    tags = models.CharField(max_length=500, blank=True, null=True)
    veiculo = models.CharField(max_length=6, blank=True, null=True)
    valor_captado_fake = models.IntegerField(blank=True, null=True)
    usar_valor_fake = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'oferta'


class Ativacao(models.Model):
    id_ativacao = models.IntegerField(primary_key=True)
    id_usuario = models.ForeignKey('Usuario', on_delete=models.CASCADE, db_column='id_usuario')
    codigo = models.CharField(max_length=100)
    data_envio = models.DateTimeField(blank=True, null=True)
    data_ativacao = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'ativacao'


class ContaBancaria(models.Model):
    id = models.AutoField(primary_key=True)
    id_conta_bancaria = models.UUIDField(default=uuid.uuid4, editable=False)
    id_investidor = models.ForeignKey(Investidor, on_delete=models.CASCADE, db_column='id_investidor', related_name='conta_bancaria')
    banco = models.CharField(max_length=50)
    agencia = models.CharField(max_length=10)
    conta = models.CharField(max_length=20)

    class Meta:
        managed = True
        db_table = 'conta_bancaria'


class EmpreendimentoSocio(models.Model):
    id_empreendimento_socio = models.AutoField(primary_key=True)
    id_empreendimento = models.ForeignKey(Empreendimento, on_delete=models.CASCADE, db_column='id_empreendimento')
    nome = models.CharField(max_length=100)
    cpf = models.CharField(max_length=11, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'empreendimento_socio'


class Funcao(models.Model):
    id_funcao = models.IntegerField(primary_key=True)
    nome = models.CharField(max_length=50)

    class Meta:
        managed = True
        db_table = 'funcao'


class Investimento(models.Model):
    id = models.AutoField(primary_key=True)
    id_investimento = models.UUIDField(default=uuid.uuid4, editable=False)
    id_investidor = models.ForeignKey(Investidor, on_delete=models.CASCADE, db_column='id_investidor')
    id_oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='oferta')
    data_criacao = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=3)
    contrato_url = models.CharField(max_length=250, blank=True, null=True)
    valor_investido = models.DecimalField(max_digits=12, decimal_places=2)
    valor_investimentos_anteriores = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    background_check = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'investimento'


class OfertaAcompanhamento(models.Model):
    id_oferta_acompanhamento = models.IntegerField(primary_key=True)
    id_oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta')
    nome_metrica = models.CharField(max_length=45, blank=True, null=True)
    valor_metrica_disponivel = models.FloatField(blank=True, null=True)
    mes = models.IntegerField(blank=True, null=True)
    mes_calendario = models.DateField(blank=True, null=True)
    valor_metrica_mes = models.FloatField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'oferta_acompanhamento'

class OfertaFavorito(models.Model):
    id_usuario_oferta_favorita = models.IntegerField(primary_key=True)
    id_oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta')
    id_usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, db_column='id_usuario')

    class Meta:
        managed = True
        db_table = 'usuario_oferta_favorita'        

class OfertaInformacoesEssenciais(models.Model):
    id_oferta_informacoes_essenciais = models.IntegerField(primary_key=True)
    id_oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='informacoes_essenciais')
    titulo = models.CharField(max_length=200, blank=True, null=True)
    texto = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'oferta_informacoes_essenciais'

class OfertaFaq(models.Model):
    id_faq = models.IntegerField(primary_key=True)
    id_oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='faq')
    pergunta = models.CharField(max_length=200,  blank=True, null=True)
    resposta = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'oferta_faq'


class OfertaContrato(models.Model):
    id_contrato = models.IntegerField(primary_key=True)
    descricao = models.CharField(max_length=500, blank=True, null=True)
    nome = models.CharField(max_length=50, blank=True, null=True)
    tipo_investidor = models.CharField(max_length=15, blank=True, null=True)
    clicksign_key = models.CharField(max_length=100, blank=True, null=True)
    dias_vencimento = models.IntegerField(blank=True, null=True)
    dias_reenvio = models.IntegerField(blank=True, null=True)
    id_oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta')

    class Meta:
        managed = True
        db_table = 'oferta_contrato'


class OfertaDocumentos(models.Model):
    id_documento = models.AutoField(primary_key=True)
    id_oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='documento')
    nome = models.CharField(max_length=70)
    arquivo_url = models.CharField(max_length=250)
    descricao = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'oferta_documentos'


class OfertaFinanceiro(models.Model):
    id = models.IntegerField(primary_key=True)
    id_oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='financeiro')
    valor_alvo_captacao_total = models.DecimalField(max_digits=20, decimal_places=4, blank=True, null=True)
    valor_alvo_captacao_minimo = models.DecimalField(max_digits=20, decimal_places=4, blank=True, null=True)
    valor_investimento_minimo = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
    valor_step = models.DecimalField(max_digits=20, decimal_places=4, blank=True, null=True)
    roi_projetado = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
    rentabilidade_projetada_anual = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
    valor_investimento_step = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
    prazo_investimento = models.IntegerField(blank=True, null=True)
    valor_metrica = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
    retorno = models.CharField(max_length=100, blank=True, null=True)
    nome_metrica = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'oferta_financeiro'


class OfertaMidia(models.Model):
    id_oferta_midia = models.AutoField(primary_key=True)
    id_oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='midia')
    nome = models.CharField(max_length=80)
    url = models.CharField(max_length=250)
    tipo_midia = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'oferta_midia'


class OfertaRemuneracao(models.Model):
    id_remuneracao = models.AutoField(primary_key=True)
    id_oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta')
    taxa_estruturacao = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
    taxa_sucesso = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
    taxa_performance = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'oferta_remuneracao'


class OfertaViabilidade(models.Model):
    id_oferta_viabilidade = models.AutoField(primary_key=True)
    id_oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='viabilidade')
    doc_indices_url = models.CharField(max_length=250, blank=True, null=True)
    indices_financeiros_html = models.CharField(max_length=2000, blank=True, null=True)
    apuracao_rentabilidade_html = models.CharField(max_length=2000, blank=True, null=True)
    linha_tempo_html = models.CharField(max_length=4000, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'oferta_viabilidade'


class UsuarioFuncao(models.Model):
    id_usuario = models.OneToOneField(Usuario, on_delete=models.CASCADE, db_column='id_usuario', primary_key=True)
    id_funcao = models.ForeignKey(Funcao, on_delete=models.CASCADE, db_column='id_funcao')

    class Meta:
        managed = True
        db_table = 'usuario_funcao'
        unique_together = (('id_usuario', 'id_funcao'),)


class UsuariosEmpresa(models.Model):
    id_usuario = models.OneToOneField(Usuario, on_delete=models.CASCADE, db_column='id_usuario', primary_key=True)
    id_empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, db_column='id_empresa')

    class Meta:
        managed = True
        db_table = 'usuarios_empresa'
        unique_together = (('id_usuario', 'id_empresa'),)
