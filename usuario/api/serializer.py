from rest_framework import serializers

from usuario.models import Usuario, Investidor, Endereco, ContaBancaria, Investimento
from oferta.api.serializer import OfertaEmpresaSimpleSerializer


class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = '__all__'

class EnderecorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Endereco
        fields = '__all__'  

class ContaBancariaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContaBancaria
        fields = '__all__'  

class InvestimentoSerializer(serializers.ModelSerializer):

    id_oferta = OfertaEmpresaSimpleSerializer(many=False, read_only=True)

    class Meta:
        model = Investimento
        fields = '__all__'          

class InvestidorSerializer(serializers.ModelSerializer):

	endereco = EnderecorSerializer(many=True, read_only=True)
	conta_bancaria = ContaBancariaSerializer(many=True, read_only=True)

	class Meta:
		model = Investidor
		fields = '__all__'


class UsuarioInvestidorSerializer(serializers.ModelSerializer):

    investidor = InvestidorSerializer(many=True, read_only=True)

    class Meta:
        model = Usuario
        fields = '__all__'             


