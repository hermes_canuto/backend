"""areadoinvestidorbackend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include

from oferta import views
from usuario.views import Logar, Investidor

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('api/usuario/', include("usuario.urls")),
    path('api/login/', Logar.as_view({'post': 'create'})),
    path('api/oferta/', views.Oferta.as_view({'get': 'list'})),
    path('api/oferta/<int:pk>/favorita', views.OfertaFavorita.as_view({ 'get': 'get','post': 'post','delete': 'delete'})),
    path('api/oferta/<int:pk>/', views.Oferta.as_view({'get': 'retrieve'})),
    path('api/oferta/<int:pk>/investir/', Investidor.as_view({'post': 'create'})),
]
