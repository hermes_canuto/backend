from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
import boto3

from oferta.api.serializer import OfertaSerializer
from oferta.api.serializer import OfertaFavoritaSerializer
from usuario.models import Oferta as OfertaModel
from usuario.models import Usuario as UsuarioDB
from usuario.models import OfertaFavorito as OfertaFavoritoDB

class Oferta(viewsets.ViewSet):

    @action(methods=['get'], detail=False)
    def list(self, request):
        queryset = OfertaModel.objects.all()
        serializer = OfertaSerializer(queryset, many=True)
        return Response(serializer.data, status.HTTP_200_OK)

    @action(methods=['get'], detail=True)
    def retrieve(self, request, pk=None):
        try:
            queryset = OfertaModel.objects.get(id=pk)
            serializer = OfertaSerializer(queryset)

            return Response(serializer.data)
        except OfertaModel.DoesNotExist:
            return Response({'Mensagem': 'Oferta Não Encontrada'}, status.HTTP_400_BAD_REQUEST)


class OfertaFavorita(viewsets.ViewSet):
    #salva oferta como favorita    
    @action(methods=['post'], detail=False)
    def post(self, request, pk=None):
        authorization = request.META.get('HTTP_AUTHORIZATION', None)

        try:
            client = boto3.client('cognito-idp')
            response = client.get_user(
                AccessToken=authorization
            )
            querysetUser = UsuarioDB.objects.get(cognitousername=response['Username'])
        except Exception as err:
            return Response({'Mensagem': 'Usuario não encontrato'}, status.HTTP_404_NOT_FOUND)

        try:    
            querysetOferta = OfertaModel.objects.get(id=pk)      
        except OfertaModel.DoesNotExist:
            return Response({'Mensagem': 'Oferta Não Encontrada'}, status.HTTP_404_NOT_FOUND)      

        f = OfertaFavoritoDB()
        f.id_oferta = querysetOferta
        f.id_usuario = querysetUser
        f.save()

        return Response({}, status.HTTP_200_OK)

    #busca se o usuario ja favoritou a oferta    
    @action(methods=['get'], detail=False)
    def get(self, request, pk=None):

        authorization = request.META.get('HTTP_AUTHORIZATION', None)

        try:
            client = boto3.client('cognito-idp')
            response = client.get_user(
                AccessToken=authorization
            )

            print(response['Username'])

            querysetUser = UsuarioDB.objects.get(cognitousername=response['Username'])
        except Exception as err:
            return Response({'Mensagem': 'Usuario não encontrato'}, status.HTTP_404_NOT_FOUND)

        try:
            queryset = OfertaFavoritoDB.objects.get(id_oferta=pk, id_usuario=querysetUser.id)
            serializer = OfertaFavoritaSerializer(queryset)

            return Response(serializer.data)
        except OfertaModel.DoesNotExist:
            return Response({'Mensagem': 'Oferta Não Encontrada'}, status.HTTP_400_BAD_REQUEST)    

    #busca se o usuario ja favoritou a oferta    
    @action(methods=['delete'], detail=False)
    def delete(self, request, pk=None):

        authorization = request.META.get('HTTP_AUTHORIZATION', None)

        try:
            client = boto3.client('cognito-idp')
            response = client.get_user(
                AccessToken=authorization
            )
            
            querysetUser = UsuarioDB.objects.get(cognitousername=response['Username'])
        except Exception as err:
            return Response({'Mensagem': 'Usuario não encontrato'}, status.HTTP_404_NOT_FOUND)

        try:
            queryset = OfertaFavoritoDB.objects.get(id_oferta=pk, id_usuario=querysetUser.id)
            queryset.delete()

            return Response({}, status.HTTP_200_OK)
        except OfertaModel.DoesNotExist:
            return Response({'Mensagem': 'Oferta Não Encontrada'}, status.HTTP_400_BAD_REQUEST)        
