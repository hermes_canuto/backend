# from django.db import models
#
# from empreendimento.models import Empreendimento, Empresa
#
# # Create your models here.
#
# class Oferta(models.Model):
#     empreendimento = models.ForeignKey(Empreendimento, on_delete=models.CASCADE, db_column='id_empreendimento', related_name='oferta')
#     nome = models.CharField(max_length=100, blank=True, null=True)
#     url_logo = models.CharField(max_length=250, blank=True, null=True)
#     site = models.CharField(max_length=45, blank=True, null=True)
#     status = models.CharField(max_length=2, blank=True, null=True)
#     data_inicio_oferta = models.DateField(blank=True, null=True)
#     data_termino_oferta = models.DateField(blank=True, null=True)
#     tags = models.CharField(max_length=500, blank=True, null=True)
#     veiculo = models.CharField(max_length=10, blank=True, null=True)
#     valor_captado_fake = models.IntegerField(blank=True, null=True)
#     descricao_html = models.CharField(max_length=400, blank=True, null=True)
#     usar_valor_fake = models.BooleanField(verbose_name="Usar Valor Fake", default=False)
#
#     def __str__(self):
#         return self.nome
#
#     objects = models.Manager()
#
#     class Meta:
#         verbose_name = "Oferta"
#         verbose_name_plural = "Ofertas"
#         ordering = ['nome']
#         db_table = 'oferta'
#
# class Financeiro(models.Model):
#     oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='financeiro')
#     valor_alvo_captacao_total = models.DecimalField(max_digits=20, decimal_places=4, blank=True, null=True)
#     valor_alvo_captacao_minimo = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
#     valor_investimento_minimo = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
#     roi_projetado = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
#     rentabilidade_projetada_anual = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
#     valor_investimento_step = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
#     prazo_investimento = models.IntegerField(blank=True, null=True)
#     valor_metrica_disponivel = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
#     nome_metrica = models.CharField(max_length=30, blank=True, null=True)
#     retorno = models.CharField(max_length=50, blank=True, null=True)
#
#     def __str__(self):
#         return self.oferta
#
#     class Meta:
#         verbose_name = "Financeiro"
#         verbose_name_plural = "Financeiros"
#         ordering = ['oferta']
#         db_table = 'oferta_financeiro'
#
#
# class Remuneracao(models.Model):
#     oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='remuneracao')
#     taxa_estruturacao = models.IntegerField(blank=True, null=True)
#     taxa_sucesso = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
#     taxa_performance = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
#
#     def __str__(self):
#         return '{%d,%s}' % (self.id, self.taxa_estruturacao)
#
#     class Meta:
#         verbose_name = "Remuneração"
#         verbose_name_plural = "Remunerações"
#         unique_together = ('oferta',)
#         ordering = ['oferta']
#         db_table = 'oferta_remuneracao'
#
#
# class Contrato(models.Model):
#     oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='contrato')
#     dias_vencimento = models.IntegerField()
#     dias_reenvio = models.IntegerField()
#     nome = models.CharField(max_length=150, blank=True, null=True)
#     descricao = models.CharField(max_length=500, blank=True, null=True)
#     mensagem_investidor = models.CharField(max_length=300, blank=True, null=True)
#     url_arquivo = models.CharField(max_length=500)
#     tipo_investidor = models.CharField(max_length=1, blank=True, null=True)
#
#     def __str__(self):
#         return self.empreendimento
#
#     class Meta:
#         verbose_name = "Contrato"
#         verbose_name_plural = "Contratos"
#         db_table = 'oferta_contrato'
#
#
# class Documento(models.Model):
#     oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='documento')
#     nome = models.CharField(max_length=70)
#     url_arquivo = models.CharField(max_length=250)
#
#     def __str__(self):
#         return self.nome
#
#     class Meta:
#         verbose_name = "Documento"
#         verbose_name_plural = "Documentos"
#         db_table = 'oferta_documento'
#
#
# class Midia(models.Model):
#     oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='midia')
#     nome = models.CharField(max_length=80)
#     url = models.CharField(max_length=250)
#     tipo_midia = models.CharField(max_length=20)
#
#     def __str__(self):
#         return self.oferta
#
#     class Meta:
#         verbose_name = "Midia"
#         verbose_name_plural = "Midias"
#         ordering = ['oferta']
#         db_table = 'oferta_midia'
#
#
# class Viabilidade(models.Model):
#     oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='viabilidade')
#     doc_indices_url = models.CharField(max_length=250)
#     indices_financeiros_html = models.CharField(max_length=2000)
#     apuracao_rentabilidade_html = models.CharField(max_length=2000)
#
#     def __str__(self):
#         return self.oferta
#
#     class Meta:
#         verbose_name = "Viabilidade"
#         verbose_name_plural = "Viabilidades"
#         ordering = ['oferta']
#         db_table = 'oferta_viabilidade'
#
#
# class Acompanhamento(models.Model):
#     oferta = models.ForeignKey(Oferta, on_delete=models.CASCADE, db_column='id_oferta', related_name='acompanhamento')
#     nome_metrica = models.CharField(max_length=45)
#     valor_metrica_disponivel = models.FloatField(blank=True, null=True)
#     mes = models.IntegerField(verbose_name="Mes", default=0)
#     mes_calendario = models.DateTimeField(blank=True, null=True)
#     valor_metrica_mes = models.FloatField(blank=True, null=True)
#
#     def __str__(self):
#         return self.oferta
#
#     class Meta:
#         verbose_name = "Acompanhamento"
#         verbose_name_plural = "Acompanhamentos"
#         ordering = ['oferta']
#         db_table = 'oferta_acompanhamento'