from rest_framework import serializers

from usuario.models import Oferta, OfertaDocumentos, OfertaMidia, OfertaViabilidade, OfertaInformacoesEssenciais, OfertaFaq, OfertaFinanceiro, OfertaFavorito, OfertaContrato
from empreendimento.api.serializer import EmpreendimentoSerializer,EmpresaSerializer


class DocumentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfertaDocumentos
        fields = '__all__'


class MidiaSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfertaMidia
        fields = '__all__'


class ViabilidadeSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfertaViabilidade
        fields = '__all__'

class InformacoesEssenciaisSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfertaInformacoesEssenciais
        fields = '__all__'

class FaqSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfertaFaq
        fields = '__all__'        

class FinanceiroSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfertaFinanceiro
        fields = '__all__'   

class FinanceiroSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfertaFinanceiro
        fields = ("id",
                  "prazo_investimento"
                  ) 


class OfertaFavoritaSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfertaFavorito
        fields = '__all__'    

class OfertaContratoSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfertaContrato
        fields = '__all__'   

class OfertaSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Oferta
        fields = '__all__'   

class OfertaEmpresaSimpleSerializer(serializers.ModelSerializer):

    id_empresa = EmpresaSerializer(many=False, read_only=True)
    financeiro = FinanceiroSimpleSerializer(many=True, read_only=True)

    class Meta:
        model = Oferta
        fields = ("id",
                  "id_oferta",
                  "nome",
                  "informacoes_essenciais",
                  "id_empresa",
                  "financeiro"
                  )





class OfertaSerializer(serializers.ModelSerializer):
    documento = DocumentoSerializer(many=True, read_only=True)
    midia = MidiaSerializer(many=True, read_only=True)
    id_empreendimento = EmpreendimentoSerializer(many=False, read_only=True)
    id_empresa = EmpresaSerializer(many=False, read_only=True)
    viabilidade = ViabilidadeSerializer(many=True, read_only=True)
    informacoes_essenciais = InformacoesEssenciaisSerializer(many=True, read_only=True)
    faq = FaqSerializer(many=True, read_only=True)
    financeiro = FinanceiroSerializer(many=True, read_only=True)

    class Meta:
        model = Oferta
        fields = ("id",
                  "id_oferta",
                  "nome",
                  "descricao_html",
                  "logo_url",
                  "site",
                  "status",
                  "data_inicio_oferta",
                  "data_termino_oferta",
                  "tags",
                  "veiculo",
                  "valor_captado_fake",
                  "usar_valor_fake",
                  "id_empresa",
                  "id_empreendimento",
                  "documento",
                  "midia",
                  "viabilidade",
                  "faq",
                  "informacoes_essenciais",
                  "financeiro"
                  )
