from rest_framework import serializers

from usuario.models import Empreendimento, Empresa


class EmpresaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empresa
        fields = '__all__'


class EmpreendimentoSerializer(serializers.ModelSerializer):
    empresa = EmpresaSerializer(many=False, read_only=True)

    class Meta:
        model = Empreendimento
        fields = '__all__'
